#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require 'csv'
require 'pp'
require 'data_mapper'
require 'dm-migrations'


""" Importa el csv pasado por parámetro a la tabla con el mismo nombre """

DataMapper::Logger.new($stdout, :debug)
DataMapper.setup(:default, "mysql://root:rober@172.31.19.219/mydb")

# importamos todos los modelos
Dir["models/*.rb"].each {|file| require_relative file }

def clearf(filename)
    filename.chomp.downcase.gsub(/[^\w\s_-]+/, '')
            .gsub(/(^|\b\s)\s+($|\s?\b)/, '\\1\\2')
            .gsub(/\s+/, '_')
end


# Normalizamos la primera linea de headers:
headers_line = File.open(ARGV[0], "r") { |f| f.readline}
normalized_headers = (headers_line.split(',').map {|x| clearf x}).join(',')
File.open(ARGV[0], "r+") {|file| file.puts normalized_headers}


# Creamos la instancia del modelo en tiempo de ejecución
csvFile = ARGV[0]
class_name = File.basename(csvFile, ".csv").capitalize
current_model = Object.const_get(class_name)


# Un par de convertidores de fecha
CSV::Converters[:mytime] = lambda{|s|
	begin
		# 11/12/77 00:00:00
		DateTime.strptime s, '%d/%m/%y %H:%M:%S'
	rescue
		s
	end
}

CSV::Converters[:mytime2] = lambda{|s|
	begin
		# 11/12/1981 00:00:00
		DateTime.strptime s, '%d/%m/%Y %H:%M:%S'
	rescue
		s
	end
}



csv = CSV.open(csvFile, :headers =>true, 
						:return_headers => false, 
						:converters=>[:all,:mytime,:mytime2], 
						:header_converters=>:symbol ) 


# Por cada línea del csv llamamos al método create de datamapper para ese modelo
csv.each do |row|
	pp row.to_hash
	current_model.create row.to_hash
end


DataMapper.finalize
DataMapper.auto_upgrade!

# Imprimimos lo que hemos insertado en la db.
current_model.send(:all).each {|r| puts r }