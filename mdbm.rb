#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require_relative 'libs/tabla'
require_relative 'libs/campo'
require_relative 'libs/makemodels'


esquema_sql = ""
esquema = Array.new

while STDIN.gets
	esquema_sql << $_
end

t = nil

""" Recorre la salida de mdb-schema y guarda en un array
instancias de tabla y ésta, a su vez, de campos """

esquema_sql.split(");").each do |e|
	e.split("\n").each do |line|
		if line.include? "CREATE TABLE"
			esquema << t unless t.nil?
			tabla = line[/\[.*?\]/].delete("[]")
			t = Tabla.new tabla
			next
		end
		unless line[/\[.*?\]/].nil?
			campo = line[/\[.*?\]/].delete("[]")
			tipo = line.split("]")[1].delete(" ,\t")
			c = Campo.new campo, tipo
			t.add_field c
		end
	end
	esquema << t unless t.nil?
end


""" Generamos los modelos """

n = MakeModels.new
n.save esquema