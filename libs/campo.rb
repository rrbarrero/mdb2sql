#!/usr/bin/env ruby
# -*- coding: utf-8 -*-


class Campo
	attr_accessor :nombre, :tipo

	def initialize nombre, tipo
		@nombre = clearf(nombre)
		@tipo = determina_tipo tipo
	end

	def get_binding
		binding
	end

	def to_s
		"#{@nombre} => #{@tipo}"
	end

	private
		def determina_tipo tipo
			if tipo.include? "Integer"
					'Integer'
			elsif tipo.include? "Text"
					'Text'
			elsif tipo.include? "DateTime"
					'DateTime'
			elsif tipo.include? "Boolean"
					'Boolean'
			elsif tipo.include? "Currency"
					'Decimal, :precision => 10, :scale =>2'
			elsif tipo.include? "Double"
					'Integer'
			elsif tipo.include? "Hyperlink"
					'Text'
			# TODO:
				# Revisar estos mapeos y añadir los que falten
				# http://fonlow.com/dbconverters/mysql2access_matrix.html
			else
				raise TypeError, "Tipo de dato desconocido '#{tipo}' para el campo #{@nombre}! \n"
			end
		end

		def clearf(filename)
    		filename.downcase.gsub(/[^\w\s_-]+/, '')
            .gsub(/(^|\b\s)\s+($|\s?\b)/, '\\1\\2')
            .gsub(/\s+/, '_')
		end
end