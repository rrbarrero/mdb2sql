#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

require 'erb'

class MakeModels
	""" Pasado un array de instancias 'tabla' renderiza su modelo """
	def initialize
		template = File.open("templates/esquema_temp.erb", "r").read
		@render = ERB.new(template)
	end

	def save tablas
		tablas.each do |t|
			@file = File.open("models/#{t.nombre}.rb", 'w')
			@file.write(@render.result(t.get_binding))
		end
	end
end