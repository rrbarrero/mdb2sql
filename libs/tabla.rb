#!/usr/bin/env ruby
# -*- coding: utf-8 -*-

class Tabla
	attr_accessor :nombre, :campos

	def initialize nombre
		@nombre = clearf(nombre)
		@campos = Array.new
	end

	def add_field(campo)
		@campos << campo
	end

	def get_binding
		binding
	end

	private
	
		def clearf(filename)
    		filename.downcase.gsub(/[^\w\s_-]+/, '')
            .gsub(/(^|\b\s)\s+($|\s?\b)/, '\\1\\2')
            .gsub(/\s+/, '_')
		end

end